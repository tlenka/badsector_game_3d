﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{

    public Camera carCamera;
    private bool isInCar;

    public event Action ClickCar = delegate { };
    public event Action ExitCar = delegate { };

    private void OnMouseDown()
    {
        ClickCar();
        isInCar = true;
        CanvasController.Get().isCameraBack = true;
    }

    public void Exit()
    {
        ExitCar();
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    Debug.Log("Exit________________ ");
        //    //CanvasController.Get().isCameraBack = true;
        //    if (isInCar)
        //    {
        //        ExitCar();
        //        isInCar = false;
        //    }
        //}

    }
}
