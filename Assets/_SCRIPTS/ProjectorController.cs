﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectorController : MonoBehaviour
{
    [SerializeField] private GameObject _Hologram;
    [SerializeField] private GameObject _Particles;
    [SerializeField] private GameObject _Cube;
    [SerializeField] private Transform _CubeStartPos;
    [SerializeField][Range(0,100)] private float _hologramRotationSpeed;


    private void Awake()
    {
        _Cube.transform.position = _CubeStartPos.position;
        _PNumbers = GetComponentsInChildren<PNumber>();
    }

    private void Update()
    {
        _Hologram.transform.Rotate(0, _hologramRotationSpeed * Time.deltaTime, 0);
    }

    private PNumber[] _PNumbers;
    private int NumText01;

    public bool IsCorrectCode()
    {
        foreach(PNumber pNumber in _PNumbers)
        {
            if (!pNumber.IsCorrect)
            {
                return false;
            }
        }
        return true;
    }

    public event Action TurnOffProjectorEvent = delegate { };

    public void CheckCode()
    {
        if (IsCorrectCode())
        {
            _Hologram.SetActive(false);
            _Particles.SetActive(false);

            TurnOffProjectorEvent();
            foreach (PNumber pNumber in _PNumbers)
            {
                pNumber.CanClick = false;
            }
        }
    }
    
}
