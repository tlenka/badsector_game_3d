﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float lookSpeed = 2f;
    private float rotationX;

    private Transform cameraTransform;
    public bool canMove = true;

    private Camera cam;
   

    private void LookAtMouse()
    {
        rotationX += -Input.GetAxis("Mouse Y");
        rotationX = Mathf.Clamp(rotationX, -40f, 40f);
        cameraTransform.transform.localEulerAngles = new Vector3(rotationX, 0, 0) * lookSpeed;
    }

    private void Awake()
    {
    }

    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        cameraTransform = GetComponentInChildren<Camera>().transform;
        rotationX = cameraTransform.localRotation.x;
        // Cursor.visible = true;
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
       // Cursor.visible = true;
       if(canMove)
            LookAtMouse();
       
    }

    
}
