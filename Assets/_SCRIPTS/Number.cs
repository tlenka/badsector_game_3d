﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Number : MonoBehaviour
{
    [SerializeField] private NumberController _NumController;

    public event Action NumberAudioEvent = delegate { };
    public int number;

    private void Start()
    {
        _TMP = GetComponent<TextMeshPro>();
    }

    private void OnMouseDown()
    {
        if (!NumberController._DoorOpened)
        {
            if (number == 9)
            {
                number = 0;
            }
            else
            {
                number++;
            }

            NumberAudioEvent();
            _TMP.text = number.ToString();
            _NumController.CheckCode();
        }
    }

    private TextMeshPro _TMP;
}
