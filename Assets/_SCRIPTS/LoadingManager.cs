﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingManager : MonoBehaviour
{
    private static LoadingManager loadingManager;
    public static LoadingManager Get()
    {
        return loadingManager;
    }

    private LoadingManager()
    {
        loadingManager = this;
    }
    
}
