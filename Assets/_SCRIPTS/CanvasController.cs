﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{
    [SerializeField] public GameObject MainPanel;
    [SerializeField] private GameObject _PausePanel;
    [SerializeField] public GameObject InventoryPanel;
    [SerializeField] private Button _newGameBtn;
    [SerializeField] private Button _quitGameBtn;
    [SerializeField] private Button _backToMenuBtn;
    [SerializeField] private Button _continueBtn;
    //private bool _MainPanelIsActive;
    private bool _gameIsRunning;
    //private bool _gameIsPause;
    public bool isCameraBack = false;
    public GameObject cursor;

    private Vector3 inventoryDefaultPosition;
    private GameScript gameScript;
    private Camera mainCamera;

    public GameObject VolumeText;


    [SerializeField] private Texture2D cursorTexture;

   // private Transform inventoryPanelParent;

    private static CanvasController _instance;
    public static CanvasController Get()
    {
        return _instance;
    }

    //CanvasController()
    //{
    //    _instance = this;
    //}

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(_instance);
        }
        _instance = this;
        DontDestroyOnLoad(this.gameObject);

        GameScript.Get().CanvasController = this;
        cursor.SetActive(false);

        inventoryDefaultPosition = InventoryPanel.transform.position;
        //inventoryPanelParent = InventoryPanel.transform.parent;

        Texture2D cursorTexture = cursor.GetComponent<RawImage>().texture as Texture2D;
        Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.ForceSoftware);
    }
    // Start is called before the first frame update
    void Start()
    {
        if (!File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            _continueBtn.gameObject.SetActive(false);
        }
        // _MainPanelIsActive = true;
        MainPanel.SetActive(true);

        _PausePanel.SetActive(false);
        _gameIsRunning = false;
       // _gameIsPause = false;
        _newGameBtn.onClick.AddListener(NewGameBtn_Click);
        _quitGameBtn.onClick.AddListener(QuitGameBtn_Click);
        _backToMenuBtn.onClick.AddListener(BackToMenuBtn_Click);
        _continueBtn.onClick.AddListener(ContinueBtn_click);
        //_newGameBtn.Click += NewGameClick_EventHandler;

        gameScript = GameScript.Get();
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("Pressed secondary button.");
            if(isInventoryOpen)
                SetInventoryOnDefaultPosition();
            else if(!isInventoryOpening)
                MoveInventoryPanel();
        }

     

        // Get the mouse position from Event.
        // Note that the y position from Event is inverted.

        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    if (isCameraBack)
        //        isCameraBack = false;
        //    else
        //    {
        //        _PausePanel.SetActive(_gameIsRunning);
        //        _gameIsRunning = !_gameIsRunning;
        //    }

        //}

    }

    private void FixedUpdate()
    {
        var mousePosition = Input.mousePosition;
        cursor.transform.position = Input.mousePosition;
    }

    private void NewGameBtn_Click()
    {
        GameScript.Get().fromFile = false;
        SceneManager.LoadScene("Level01", LoadSceneMode.Single);
        InventoryPanel.SetActive(true);
        //GameScript.Get().LoadGame(false);
        //_gameIsRunning = true;
        //MainPanel.SetActive(false);
    }

    private void QuitGameBtn_Click()
    {
        Application.Quit();
    }

    private void BackToMenuBtn_Click()
    {
        Debug.Log("BackToMenuBtn_Click _________________ ");
        _gameIsRunning = false;
        gameScript.SaveGame();
        _PausePanel.SetActive(false);
        Inventory.Get().CollectedItems.Clear();
        foreach (var slot in Inventory.Get()._Slots)
            Destroy(slot);

        Destroy(GameScript.Get());
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);

        //_PausePanel.SetActive(false);
        //MainPanel.SetActive(true);
        //_continueBtn.gameObject.SetActive(true);
    }

    private void ContinueBtn_click()
    {
        GameScript.Get().fromFile = true;
        SceneManager.LoadScene("Level01", LoadSceneMode.Single);
       // GameScript.Get().LoadGame(true);
       // _gameIsRunning = true;
       // _MainPanel.SetActive(false);
    }

    private void MoveInventoryPanel()
    {
       // var positionY = InventoryPanel.transform.position.y - (Screen.height / 2 - 36);
       // var position = new Vector3(InventoryPanel.transform.position.x, positionY, InventoryPanel.transform.position.z);
        //InventoryPanel.transform.Translate(position, Space.Self);

        //Vector3 newVector3 = Vector3.Lerp(inventoryDefaultPosition, position, Time.deltaTime * 0.8f);
        //InventoryPanel.transform.position = Vector3.Lerp(inventoryDefaultPosition, position, Time.deltaTime * 0.8f);

        //InventoryPanel.transform.position = position;

        StartCoroutine(OpenInventoryPanel());
        gameScript.Player.canMove = false;
        gameScript.Player.cameraController.canMove = false;
        UnlockCursor();
    }

    private bool isInventoryOpen = false;
    private bool isInventoryOpening = false;
    IEnumerator OpenInventoryPanel()
    {
        foreach(var slot in Inventory.Get()._Slots)
        {
            slot.transform.SetParent(InventoryPanel.transform);
        }
        VolumeText.transform.SetParent(InventoryPanel.transform);

        isInventoryOpening = true;
        var positionY = InventoryPanel.transform.position.y - (Screen.height / 2 - 36);
        var position = new Vector3(InventoryPanel.transform.position.x, positionY, 0);
        float time = 0f;

        while (InventoryPanel.transform.position.y > positionY)
        {
            time += Time.deltaTime * 6f ;
            InventoryPanel.transform.position = Vector3.Lerp(inventoryDefaultPosition, position, time);
            yield return null;
        }

        foreach (var slot in Inventory.Get()._Slots)
        {
            slot.transform.SetParent(InventoryPanel.transform.parent);
        }
        VolumeText.transform.SetParent(InventoryPanel.transform.parent);

        isInventoryOpening = false;
        isInventoryOpen = true;
        
        yield return null;
    }

    private void SetInventoryOnDefaultPosition()
    {
        foreach (var slot in Inventory.Get()._Slots)
        {
            slot.transform.SetParent(InventoryPanel.transform);
        }
        VolumeText.transform.SetParent(InventoryPanel.transform);

        InventoryPanel.transform.position = inventoryDefaultPosition;
        isInventoryOpen = false;
        foreach (var slot in Inventory.Get()._Slots)
        {
            slot.transform.SetParent(InventoryPanel.transform.parent);
        }
        VolumeText.transform.SetParent(InventoryPanel.transform.parent);

        gameScript.Player.canMove = true;
        gameScript.Player.cameraController.canMove = true;
        LockCursor();
    }
    private void UnlockCursor()
    {
        Cursor.lockState = CursorLockMode.None;
    }

    private void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private bool isPauseMenuActive = false;

    public void TogglePauseMenu()
    {
        if (isPauseMenuActive)
        {
            _PausePanel.SetActive(false);
            isPauseMenuActive = false;
            gameScript.Player.canMove = true;
            gameScript.Player.cameraController.canMove = true;
            LockCursor();
            InventoryPanel.SetActive(true);
            foreach (var slot in Inventory.Get()._Slots)
            {
                slot.transform.SetParent(InventoryPanel.transform.parent);
            }
            VolumeText.transform.SetParent(InventoryPanel.transform.parent);
        }
        else
        {
            _PausePanel.SetActive(true);
            isPauseMenuActive = true;
            gameScript.Player.canMove = false;
            gameScript.Player.cameraController.canMove = false;
            UnlockCursor();
            foreach (var slot in Inventory.Get()._Slots)
            {
                slot.transform.SetParent(InventoryPanel.transform);
            }
            VolumeText.transform.SetParent(InventoryPanel.transform);
            InventoryPanel.SetActive(false);

        }
    }
}
