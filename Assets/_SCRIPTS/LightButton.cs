﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightButton : MonoBehaviour
{
    [SerializeField] private BatteryController _Battery;
    [SerializeField] private GameObject _ButteryLight;
    
    private void Awake()
    {
        _Light = _ButteryLight.GetComponent<Light>();
    }
    void Start()
    {
        _Light.enabled = false;
    }

    private void OnMouseDown()
    {
        bool light = _Light.enabled ? (_Light.enabled = false) : (_Light.enabled = true);
       
        foreach (CubeItem cube in _Battery._CubeItems)
        {
            if (!cube.Item.CanClick)
            {
                cube.Number.gameObject.SetActive(light);
            }
        }
    }

    private Light _Light;
}
