﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class ComputerStationText : MonoBehaviour
{
    public event Action TurnOnComputerStationEvent = delegate { };
    private void Awake()
    {
        _Text = GetComponent<TextMeshPro>();
        _Text.enabled = false;
    }

    private void OnMouseDown()
    {
        _Text.enabled = true;
        TurnOnComputerStationEvent();
    }

    private TextMeshPro _Text;
}
