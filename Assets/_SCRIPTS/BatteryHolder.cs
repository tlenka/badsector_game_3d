﻿using UnityEngine;

public class BatteryHolder : MonoBehaviour
{
    [SerializeField] private BatteryController _Battery;

    public GameObject _CorrectCubeItem;
    

    private void Start()
    {
        _CubeItems = _Battery._CubeItems;
    }

    private void OnMouseDown()
    {
        foreach (CubeItem cube in _CubeItems)
        {
            //var item= cube.gameObject.GetComponent<ItemToCollect>();

            if (cube.Item.IsSelected)
            {
                cube.gameObject.transform.position = transform.position;
                cube.gameObject.transform.rotation = transform.parent.rotation;
                cube.Item.IsAppearing = true;

                cube.gameObject.SetActive(true);

                if (cube.gameObject.Equals(_CorrectCubeItem))
                {
                    cube.Item.CanClick = false;

                    if (_Battery.gameObject.GetComponentInChildren<Light>().isActiveAndEnabled)
                    {
                        cube.Number.gameObject.SetActive(true);
                    }
                }
            }
        }
    }

    private CubeItem[] _CubeItems;
    private ItemToCollect[] _Items;
}
