﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCar : MonoBehaviour
{

    private bool isMoving = false;
    private Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        direction = transform.position/* + new Vector3(0, 0, 4f)*/;
    }

    private bool isMoveForward = true;
    private bool isMoveLeft;
    private bool isMoveRight;

    private bool isMoveBack;


    // Update is called once per frame
    void Update()
    {
        if(transform.position.z > -243.1f)
        {
            isMoveForward = false;
            isMoveLeft = true;
           //if( transform.rotation.y > -90f)
            if (transform.position.z < -241.5f )
            {
                direction.z = -241.6f;
                direction.x -= 0.4f;

                transform.localEulerAngles -= new Vector3(0, 9f, 0);
            }
            else
            {
                //transform.localEulerAngles = new Vector3(0, -90, 0);
               // direction = transform.position;
               
            }
        }

        if (transform.position.x < -10.7f)
        {
            isMoveLeft = false;
            isMoveBack = true;
        }


        transform.position = Vector3.Lerp(transform.position, direction, Time.deltaTime * 2f);

        //if (isMoving)
        //{
        //    transform.position = Vector3.Lerp(transform.position, Vector3.forward * 0.1f, Time.deltaTime * 1.1f);
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        if(isMoveForward)
            direction = transform.position + new Vector3(0, 0, 5f);
        if(isMoveLeft)
            direction = transform.position + new Vector3(-5, 0, 0f);
        if (isMoveBack)
            direction = transform.position + new Vector3(0, 0, -5f);
        if (isMoveRight)
            direction = transform.position + new Vector3(5, 0, 0f);

    }
}
