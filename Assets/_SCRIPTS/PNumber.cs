﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class PNumber : MonoBehaviour
{
    [SerializeField] private int _CorrectNum;

    public TextMeshPro NumberText;
    public bool CanClick = true;
    public bool IsCorrect;

    private void Awake()
    {
        NumberText = GetComponentInChildren<TextMeshPro>();
        NumberText.text = _Number.ToString();
        _PController = GetComponentInParent<ProjectorController>();
    }


    public event Action ClickEvent = delegate { };

    private void OnMouseDown()
    {
        if (CanClick)
        {
            if (_Number == 9)
            {
                _Number = 0;
            }
            else
            {
                _Number++;
            }
            NumberText.text = _Number.ToString();

            if (_Number == _CorrectNum)
            {
                IsCorrect = true;
                _PController.CheckCode();
            }
            else
            {
                IsCorrect = false;
            }

            ClickEvent();
        }
    }

    private int _Number;
    private ProjectorController _PController;
}
