﻿using UnityEngine;

public class PlayerAudioManager : MonoBehaviour
{
    [SerializeField] private PlayerController _player;
    [SerializeField] private AudioSource _playerAudioSource;
    [SerializeField] private AudioClip _footStepAudioClip;
    [SerializeField] private AudioClip _rotationAudioClip;

    void Start()
    {
        _player.FootStepAudioEvent += FootStepAudioEvent_EventHandler;
        _player.RotationAudioEvent += RotationAudioEvent_EventHandler;
    }

    private void RotationAudioEvent_EventHandler()
    {
        _playerAudioSource.PlayOneShot(_rotationAudioClip);
    }

    private void FootStepAudioEvent_EventHandler()
    {
        _playerAudioSource.PlayOneShot(_footStepAudioClip);
    }
}
