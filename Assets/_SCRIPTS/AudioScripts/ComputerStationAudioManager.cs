﻿using UnityEngine;

public class ComputerStationAudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource _turnOnComputerStationAudioSource;
    [SerializeField] private AudioClip _turnOnComputerStationAudioClip;
    private ComputerStationText _computerStation;

    private void OnEnable()
    {
        _computerStation = FindObjectOfType<ComputerStationText>();
        _computerStation.TurnOnComputerStationEvent += TurnOnComputerStationEvent_EventHandler;
    }

    private void TurnOnComputerStationEvent_EventHandler()
    {
        _turnOnComputerStationAudioSource.PlayOneShot(_turnOnComputerStationAudioClip);
    }
}
