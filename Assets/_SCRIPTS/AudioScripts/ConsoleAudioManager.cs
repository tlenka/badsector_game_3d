﻿using UnityEngine;
using UnityEngine.Audio;

public class ConsoleAudioManager : MonoBehaviour
{
    [SerializeField] private AudioMixerSnapshot _isNotPlaying;
    [SerializeField] private AudioMixerSnapshot _isPlaying;
    [SerializeField] private AudioMixerSnapshot _isOnTheLine;
    [SerializeField] [Range(0, 10)] private float _snapshotsTransition = 1.2f;

    private int _snapSwitch;
    private PointerMiniGame _pointerMiniGame;
    private PlayButton _playButton;

    private void OnEnable()
    {
        _pointerMiniGame = FindObjectOfType<PointerMiniGame>();
        _playButton = FindObjectOfType<PlayButton>();
        _pointerMiniGame.SwitchSnapshotEvent += SwitchSnapshotEvent_EventHandler;
        _playButton.SwitchSnapshotEvent += SwitchSnapshotEvent_EventHandler;
        _snapSwitch = 0;
    }

    void SwitchSnapshotEvent_EventHandler(int snapSwitchNumber = 0)
    {
        switch (snapSwitchNumber)
        {
            case 0:
                _isNotPlaying.TransitionTo(_snapshotsTransition);
                break;
            case 1:
                _isPlaying.TransitionTo(_snapshotsTransition);
                break;
            case 2:
                _isOnTheLine.TransitionTo(_snapshotsTransition);
                break;
        }
    }
}
