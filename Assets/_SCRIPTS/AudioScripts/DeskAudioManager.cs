﻿using UnityEngine;

public class DeskAudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource _deskAudioSource;
    [SerializeField] private AudioClip _numAudioClip;
    private Number[] _nums;

    private void OnEnable()
    {
        _nums = FindObjectsOfType<Number>();
        foreach(Number num in _nums)
        {
            num.NumberAudioEvent += NumberAudioEvent_EventHandler;
        }
    }

    private void NumberAudioEvent_EventHandler()
    {
        _deskAudioSource.PlayOneShot(_numAudioClip);
    }
}
