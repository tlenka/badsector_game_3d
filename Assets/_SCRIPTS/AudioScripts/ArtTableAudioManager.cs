﻿using UnityEngine;
using UnityEngine.Audio;

public class ArtTableAudioManager : MonoBehaviour
{
    [SerializeField] private ArtTableController _artTable;
    [SerializeField] private AudioSource _artTableAudioSource;
    [SerializeField] private AudioSource _cubeMoveUpAudioSource;
    [SerializeField] private AudioClip _putOnHolderAudioClip;
    private Holder[] _holders;
    
    private void OnEnable()
    {
        _holders = FindObjectsOfType<Holder>();
        foreach(Holder holder in _holders)
        {
            holder.PutOnHolderEvent += PutOnHolderEvent_EventHandler;
        }
        _artTable.CubeMoveUpEvent += CubeMoveUpEvent_EventHandler;
    }

    void PutOnHolderEvent_EventHandler(AudioMixerSnapshot snapshot, float transistionTo)
    {
        snapshot.TransitionTo(transistionTo);
    }

    void CubeMoveUpEvent_EventHandler()
    {
        _cubeMoveUpAudioSource.Play();
    }
}
