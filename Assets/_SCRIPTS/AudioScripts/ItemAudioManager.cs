﻿using UnityEngine;

public class ItemAudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSouceT;
    [SerializeField] private AudioClip _grabItemAudioClip;
    [SerializeField] private AudioClip _putItemAudioClip;

    private ItemToCollect[] _itemsToCollect;
    private Holder[] _artTableHolders;

    void Start()
    {
        _itemsToCollect = FindObjectsOfType<ItemToCollect>();
        foreach (ItemToCollect item in _itemsToCollect)
        {
            item.GrabItemEvent += GrabItemEvent_EventHandler;
        }

        _artTableHolders = FindObjectsOfType<Holder>();
        foreach(Holder _holder in _artTableHolders)
        {
            _holder.PutItemEvent += PutItemEvent_EventHandler;
        }
    }

    void GrabItemEvent_EventHandler()
    {
        _audioSouceT.PlayOneShot(_grabItemAudioClip);
    }

    void PutItemEvent_EventHandler()
    {
        _audioSouceT.PlayOneShot(_putItemAudioClip);
    }
}
