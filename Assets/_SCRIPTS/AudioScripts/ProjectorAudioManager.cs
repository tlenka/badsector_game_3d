﻿using UnityEngine;

public class ProjectorAudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource _projectorAudioSource;
    [SerializeField] private AudioSource _pLockAudioSource;

    [SerializeField] private AudioClip _projectorAudioClip;
    [SerializeField] private AudioClip _clickAudioClip;

    [SerializeField] private ProjectorController _projector;

    private PNumber[] pNumbers;

    private void OnEnable()
    {
        pNumbers = FindObjectsOfType<PNumber>();
        foreach (PNumber pNum in pNumbers)
        {
            pNum.ClickEvent += ClickEvent_EventHandler;
        }

        _projector.TurnOffProjectorEvent += TurnOffProjectorEvent_EventHandler;
    }

    private void TurnOffProjectorEvent_EventHandler()
    {
        _projectorAudioSource.enabled = false;
    }

    private void ClickEvent_EventHandler()
    {
        _pLockAudioSource.PlayOneShot(_clickAudioClip);
    }
}
