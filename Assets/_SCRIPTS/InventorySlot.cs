﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class InventorySlot : MonoBehaviour, IPointerClickHandler
{
    public ItemToCollect Item;
    public Image ItemImage;
    private bool _SlotSelected;
    private ItemToCollect[] _ItemToCollect;
    private InventorySlot[] _Slots;
    //public Sprite Image;
   // public Sprite _Img;

    private void Start()
    {
        ItemImage = GetComponent<Image>();
        _ItemToCollect = FindObjectsOfType<ItemToCollect>();
        _Slots = FindObjectsOfType<InventorySlot>();

        Color itemImgColor = Color.black;
        itemImgColor.a = 0f;
        ItemImage.color = itemImgColor;
        
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        if (Item != null)
        {
            Color color = ItemImage.color;
         
            foreach (ItemToCollect item in _ItemToCollect)
                {
                    item.IsSelected = false;

                }

            foreach (InventorySlot slot in _Slots)
                {
                    if (slot.Item != null)
                    {
                        slot.ItemImage.sprite = slot.Item.Image;
                        color.a = 1f;
                        slot.ItemImage.color = color;
                }
            }

            Item.IsSelected = true;
            color.a = 0.5f;
            ItemImage.color = color;
        }

    }

   

}
