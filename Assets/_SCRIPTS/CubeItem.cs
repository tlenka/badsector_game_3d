﻿using UnityEngine;
using TMPro;


public class CubeItem : MonoBehaviour
{
   
    public TextMeshPro Number;
    public ItemToCollect Item;

    //private Material _apearingItem;
    //[SerializeField] [Range(1f, 10f)] private float _appearingSpeed;
    //private float _appearingStep;

    void Start()
    {
        Item = gameObject.GetComponent<ItemToCollect>();
       // _CubeItems = FindObjectsOfType<CubeItem>();
        Number = gameObject.GetComponentInChildren<TextMeshPro>();
        Number.gameObject.SetActive(false);

        //_appearingStep = 1f;

        //_apearingItem = gameObject.GetComponent<Renderer>().material;
        //_apearingItem.SetFloat("Vector1_C27F7F7A", _appearingStep);
    }

    private void Update()
    {
        //if (_isDisappearing)
        //{
        //    if (_appearingStep >= 0)
        //    {
        //        _apearingItem.SetFloat("Vector1_C27F7F7A", _appearingStep);
        //        _appearingStep -= 0.01f;
        //    }
        //    else
        //    {
        //        _isDisappearing = false;
        //        gameObject.SetActive(false);
        //        _appearingStep = 0;
        //    }

        //}

        //if (IsAppearing)
        //{
            
        //    if (_appearingStep < 1)
        //    {
        //        _apearingItem.SetFloat("Vector1_C27F7F7A", _appearingStep);
        //        _appearingStep += 0.01f;
        //    }
        //    else
        //    {
        //        IsAppearing = false;
        //    }

        //}
    }


    //public bool IsAppearing;
    //private bool _isDisappearing;

    private void OnMouseDown()
    {
        if (Item.CanClick)
        {
            //_isDisappearing = true;
            
        }
    }


    //private CubeItem[] _CubeItems;
}
