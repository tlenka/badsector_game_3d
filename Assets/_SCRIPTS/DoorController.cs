﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
   

    private void Start()
    {
        _DoorAnimator = GetComponent<Animator>();
    }

    private void OnMouseDown()
    {
        if (NumberController._DoorOpened)
        {
            _DoorAnimator.Play("door_2_open", 0, 0.1f);
        }
    }

    private Animator _DoorAnimator;
}
