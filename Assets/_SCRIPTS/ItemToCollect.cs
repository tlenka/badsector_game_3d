﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ItemToCollect : MonoBehaviour
{
    public int index;
    [SerializeField] public bool IsSelected;
   
    public bool CanClick = true;
    public Sprite Image;
    public bool IsCollected;

    public event Action GrabItemEvent = delegate { };

    

    private Inventory _Invetory;
    private AudioSource _itemAudioSource;

    private Material _apearingItem;
    [SerializeField] [Range(1f, 10f)] private float _appearingSpeed = 2f;
    private float _appearingStep;

    public Vector3 originalPosition;
    public Quaternion originalRotation;

    public bool isArtItem = false;
    private void Awake()
    {
        originalPosition = this.gameObject.transform.position;
        originalRotation = this.gameObject.transform.rotation;

    }
    private void Start()
    {
        //_Invetory = Inventory.Get();
        //Debug.Log(_Invetory.name);
       // _Invetory = FindObjectOfType<Inventory>();

        //_itemAudioSource = gameObject.AddComponent<AudioSource>();
        //_itemAudioSource = GetComponent<AudioSource>();

        _appearingStep = 1f;

        _apearingItem = gameObject.GetComponent<Renderer>().material;
        _apearingItem.SetFloat("Vector1_C27F7F7A", _appearingStep);

        if (GetComponent<ArtItem>())
            isArtItem = true;
    }

    private void Update()
    {
        if (isArtItem)
        {
            if (_isDisappearing)
            {
                Debug.Log(transform.localScale.x + " ----------- ");
                if(transform.localScale.x > 0.2f)
                {
                    var scaleX = transform.localScale.x * 0.8f;
                    var scaleY = transform.localScale.y * 0.8f;
                    var scaleZ = transform.localScale.z * 0.8f;
                    transform.localScale = new Vector3(scaleX, scaleY, scaleZ);

                    transform.eulerAngles = new Vector3(transform.rotation.x, transform.rotation.y - 20f, transform.rotation.z);
                   // transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y - 1f, transform.rotation.z);
                }
                else
                {
                    _isDisappearing = false;
                    gameObject.SetActive(false);
                    transform.localScale = Vector3.one;
                }

            }

            if (IsAppearing)
            {

                if (_appearingStep < 1)
                {
                    _apearingItem.SetFloat("Vector1_C27F7F7A", _appearingStep);
                    _appearingStep += 0.01f;
                }
                else
                {
                    IsAppearing = false;
                }

            }
        }
        else
        {
            if (_isDisappearing)
            {
                if (_appearingStep >= 0)
                {
                    _apearingItem.SetFloat("Vector1_C27F7F7A", _appearingStep);
                    _appearingStep -= 0.01f;
                }
                else
                {
                    _isDisappearing = false;
                    gameObject.SetActive(false);
                    _appearingStep = 0;
                }

            }

            if (IsAppearing)
            {

                if (_appearingStep < 1)
                {
                    _apearingItem.SetFloat("Vector1_C27F7F7A", _appearingStep);
                    _appearingStep += 0.01f;
                }
                else
                {
                    IsAppearing = false;
                }

            }
        }
       


    }

    public bool IsAppearing;
    private bool _isDisappearing;

    private void OnMouseDown()
    {
        if (CanClick)
        {
            _isDisappearing = true;    

            GrabItemEvent();
            //gameObject.SetActive(false);
            //IsSelected = true;
            _Invetory = Inventory.Get();
            _Invetory.AddItem(this);
            Debug.Log(_Invetory.name);
        }


    }

    public void ReloadMaterial()
    {
        _apearingItem.SetFloat("Vector1_C27F7F7A", 1f);
    }



}
