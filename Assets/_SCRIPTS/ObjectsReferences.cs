﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectsReferences : MonoBehaviour
{
    public GameObject Fader;
    public List<ItemToCollect> ItemsToCollect = new List<ItemToCollect>();

    private void Awake()
    {
        GameScript.Get().ObjectsReferences = this;
    }
}
