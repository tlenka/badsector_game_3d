﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenController : MonoBehaviour
{
    private void Start()
    {
        _ScreenChildren = GetComponentsInChildren<Transform>(true);

        foreach(Transform child in _ScreenChildren)
        {
            if (child != gameObject.transform)
            {
                child.gameObject.SetActive(false);
            }
        }
    }

    private void OnMouseDown()
    {
        foreach (Transform child in _ScreenChildren)
        {
                child.gameObject.SetActive(true);
        }
    }

    private Transform[] _ScreenChildren;

}
