﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton : MonoBehaviour
{
    [SerializeField] public GameObject _Way;

    public bool GamePlay;

    public event Action<int> SwitchSnapshotEvent = delegate { };

    private void Awake()
    {
        _Way.SetActive(false);
    }
    private void OnMouseDown()
    {
        gameObject.SetActive(false);
        GamePlay = true;
        _Way.SetActive(true);
        SwitchSnapshotEvent(2);
    }
}
