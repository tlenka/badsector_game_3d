﻿using System;
using UnityEngine;
using UnityEngine.Audio;

public class ArtTableController : MonoBehaviour
{
    [SerializeField] private CubeItem _Cube;

    public float speed = 1.0F;
    public ItemToCollect[] ArtItems { get; private set; }

    //private ArtItem[] ArtItems { get; set; }

    //public ArtTableController()
    //{
    //    this._isCompleted = false;
    //}
    private void Awake()
    {
        //IsCompleted = false;



        _holders = GetComponentsInChildren<Holder>();
        ArtItems = new ItemToCollect[_holders.Length];

        for (int i = 0; i < _holders.Length; i++)
        {
            ArtItems[i] = _holders[i]._CorrectArtItem;

            //_holders[i].PutOnHolderEvent += PutOnHolderEvent_EventHandler;
        }
        
        _StartTime = Time.time;
        _Cube.transform.position = gameObject.transform.position + new Vector3(0, gameObject.transform.lossyScale.y, 0) - new Vector3(0, _Cube.transform.lossyScale.y/2, 0);
        _EndPos = gameObject.transform.position + new Vector3(0, gameObject.transform.lossyScale.y, 0) + new Vector3(0, _Cube.transform.lossyScale.y, 0);

        ItemToCollect cubeItem = _Cube.GetComponent<ItemToCollect>();
        cubeItem.originalPosition = _Cube.transform.position;
    }


    void Update()
    {
        if (IsCompletedCheck() && _CubeCanMove)
        {
            CubeMoveUp();
        }

        //_isCompleted = !_isCompleted;
        //Debug.Log(IsCompleted);
    }

    private Vector3 _StartPos;
    private Vector3 _EndPos;
    private float _StartTime;

    private float _JourneyLength;
    private bool _CubeCanMove = true;
    private Holder[] _holders;


    //private void PutOnHolderEvent_EventHandler(AudioMixerSnapshot arg1, float arg2)
    //{
        
    //}

    private bool _isCompleted = false;
    public bool IsCompleted
    {
        get
        {
            return _isCompleted;

        }
        set
        {
            if (_isCompleted != value)
            {
                _isCompleted = value;
                IsCompletedOnChanged();
            }

        }
    }

    protected void IsCompletedOnChanged()
    {
        CubeMoveUpEvent();
        BlockAllArtItems();
    }


    public bool IsCompletedCheck()
    {
        if (!IsCompleted)
        {
            foreach (Holder holder in _holders)
            {
                if (!holder.IsCorrect)
                {
                    return false;
                }
            }
            IsCompleted = true;
        }

        return true;

    }

    public event Action CubeMoveUpEvent = delegate { };

    public void CubeMoveUp()
    {
        
        if (_Cube.transform.position == _EndPos)
        {
            _CubeCanMove = false;
        }
        else
        {
            _Cube.transform.position = Vector3.MoveTowards(_Cube.transform.position, _EndPos, Time.deltaTime * 0.5f);
        }
    }

    void BlockAllArtItems()
    {
        foreach (ItemToCollect artItem in ArtItems)
        {
            artItem.CanClick = false;
        }
    }


}
