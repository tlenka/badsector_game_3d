﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class GameScript : MonoBehaviour
{
    public PlayerController Player;
    private Inventory inventory;
    public Fader Fader;
    public bool fromFile = false;

    public bool IsSceneLoaded = false;
    public ObjectsReferences ObjectsReferences;
    public CanvasController CanvasController;

    private static GameScript _gameScript;
    public static GameScript Get()
    {
        return _gameScript;
    }

   
    //private GameScript()
    //{
    //    if (_gameScript == null)
    //        _gameScript = this;
    //    else
    //        Destroy(this);
    //}

    private void Awake()
    {
        if (_gameScript != null && _gameScript != this)
            Destroy(this.gameObject);
        else
        {
            _gameScript = this;
            DontDestroyOnLoad(this.gameObject);
        }

    }
    void Start()
    {
        inventory = Inventory.Get();
        //File.Delete(Application.persistentDataPath + "/gamesave.save");
    }

    public void SaveGame()
    {
        Save save = CreateSaveGameObject();
        //JSONString = JsonUtility.ToJson(save);
        //File.WriteAllText(Application.persistentDataPath + "/SaveFile.json", JSONString);
        //Debug.Log("Saved " + Application.persistentDataPath);
        // 1
        //Save save = CreateSaveGameObject();
        ////Debug.Log(Application.persistentDataPath);
        //// 2
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, save);
        file.Close();
        Debug.Log("Saved " + Application.persistentDataPath);
        
    }


    public void LoadGame(bool fromFile)
    {
        PrepareInventory();

        if (fromFile)
        {
            if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);

                Save save = (Save)bf.Deserialize(file);
                file.Close();

                // 3
                for (int i = 0; i < save.itemsToCollectIndexes.Count; i++)
                {
                    int index = save.itemsToCollectIndexes[i];
                    ItemToCollect item = Inventory.Get().ItemsToCollect[index];
                    float[] coordinates = save.collectedItemPosition[i];
                    float[] rotCoordinates = save.collectedItemRotation[i];

                    item.transform.position = new Vector3(coordinates[0], coordinates[1], coordinates[2]);
                    item.transform.rotation = Quaternion.Euler(rotCoordinates[0], rotCoordinates[1], rotCoordinates[2]);

                    if (save.isCollected[i] == 1)
                    {
                        Inventory.Get().AddItem(item);
                        item.IsCollected = true;
                        item.gameObject.SetActive(false);
                    }
                }

                Player.transform.position = new Vector3(save.playerPosition[0], save.playerPosition[1], save.playerPosition[2]);
                Player.transform.rotation = Quaternion.Euler(save.playerRotation[0], save.playerRotation[1], save.playerRotation[2]);
            }
            else
            {
                Debug.Log("No game saved! Starting new Game");
                LoadGame(false);
            }
        }
        else
        {
            Debug.Log("New Game");
        }


        Debug.Log("Load Game __________________ " + CanvasController.Get().MainPanel.activeInHierarchy);
        PrepareSceneObjects();
        CanvasController.Get().MainPanel.SetActive(false);
        StartTheGame();
    }

    private void StartTheGame()
    {
        Fader.Fade();
       // CanvasController.cursor.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void PrepareSceneObjects()
    {
        //Fader = ObjectsReferences.Fader.GetComponent<Fader>();
        //while (Fader == null)
        //    continue;

        Vector3 faderPosition = Player.transform.position;
        Quaternion faderRotation = Player.transform.rotation;
        Vector3 faderVectorForward = faderRotation * Vector3.forward;

        Fader.transform.rotation = faderRotation;
        Fader.transform.position = new Vector3(faderPosition.x + faderVectorForward.x * 16f, faderPosition.y, faderPosition.z + faderVectorForward.z * 16f);
        Fader.gameObject.SetActive(true);

    }

    private Save CreateSaveGameObject()
    {
        Save save = new Save();

        foreach (ItemToCollect itemToCollect in Inventory.Get().ItemsToCollect)
        {
            save.itemsToCollectIndexes.Add(itemToCollect.index);
            float[] position = new float[3] { itemToCollect.transform.position.x, itemToCollect.transform.position.y, itemToCollect.transform.position.z };
            float[] rotation = new float[3] { itemToCollect.transform.eulerAngles.x, itemToCollect.transform.eulerAngles.y, itemToCollect.transform.eulerAngles.z };

            save.collectedItemPosition.Add(position);
            save.collectedItemRotation.Add(rotation);

            if (itemToCollect.IsCollected) save.isCollected.Add(1);
            else save.isCollected.Add(0);
        }

        save.playerPosition = new float[3] { Player.transform.position.x, Player.transform.position.y, Player.transform.position.z };
        save.playerRotation = new float[3] { Player.transform.eulerAngles.x, Player.transform.eulerAngles.y, Player.transform.eulerAngles.z };

        //Debug.Log(save.playerRotation[0] + " ___________ " + save.playerRotation[1] + " ___________ " + save.playerRotation[2]);
        return save;
    }

    public void ResetTransforms()
    {
        foreach(ItemToCollect item in Inventory.Get().ItemsToCollect)
        {
            item.transform.position = item.originalPosition;
            item.transform.rotation = item.originalRotation;
            item.gameObject.SetActive(true);
            item.ReloadMaterial();
        }

        Player.transform.position = Player.startPosition;
        Player.transform.rotation = Player.startRotation;
    }

    public void ResetInventory()
    {
        List<ItemToCollect> collectedItems = Inventory.Get().CollectedItems;
        foreach (ItemToCollect item in collectedItems)
        {
            item.IsCollected = false;

        }
        collectedItems.Clear();

        foreach (InventorySlot slot in Inventory.Get()._Slots)
        {

            //var color = slot.ItemImage.color;
            var color = Color.black;
            color.a = 0f;
            
            slot.ItemImage.color = color;

            slot.Item = null;
            slot.ItemImage.sprite = null;
           
        }
    }

    private void PrepareInventory()
    {
        inventory.ItemsToCollect = ObjectsReferences.ItemsToCollect;
        inventory.Prepare();
    }
}
