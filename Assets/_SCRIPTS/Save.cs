﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Save
{
    public List<int> itemsToCollectIndexes = new List<int>();
    public List<int> isCollected = new List<int>();
    public List<float[]> collectedItemPosition = new List<float[]>();
    public List<float[]> collectedItemRotation = new List<float[]>();

    public float[] playerPosition;
    public float[] playerRotation;


}
