﻿using UnityEngine;

public class BatteryController : MonoBehaviour
{
    public CubeItem[] _CubeItems;
    
    private void Awake()
    {
        _CubeItems = new CubeItem[3];
        _holders = GetComponentsInChildren<BatteryHolder>();

        for(int i= 0; i < _holders.Length; i++)
        {
            _CubeItems[i] = _holders[i]._CorrectCubeItem.GetComponent<CubeItem>();
        }
    }

    private BatteryHolder[] _holders;
}
