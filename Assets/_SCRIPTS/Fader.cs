﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fader : MonoBehaviour
{
    private Renderer renderer;
    private Color color;

    Color colorStart = Color.red;
    //Color colorEnd = Color.green;
    float duration = 1.0f;
    private bool isFading = false;
    public void Awake()
    {
        renderer = GetComponent<Renderer>();
        color = new Color32(0, 0, 0, 255);
        renderer.material.SetColor("_BaseColor", color);

        GameScript.Get().Fader = this;
        //gameObject.SetActive(false);

    }

    public void Fade()
    {
        if (!isFading)
            StartCoroutine(Fading());
    }
    public IEnumerator Fading()
    {
        isFading = true;
        while(color.a > 0f)
        {
            color.a -= 0.18f * Time.deltaTime; 
            renderer.material.SetColor("_BaseColor", color);
            transform.Translate(Vector3.forward * Time.deltaTime * 2.0f);
            yield return null;
        }

        isFading = false;
        gameObject.SetActive(false);
        color.a = 1f;
        renderer.material.SetColor("_BaseColor", color);
    }
}
