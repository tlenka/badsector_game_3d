﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class PointerMiniGame : MonoBehaviour
{
    [SerializeField] private Camera _pointerGameCamera;
    //[SerializeField] private Button _backButton;
    [SerializeField] private PlayButton _PlayButton;

    public event Action<int> SwitchSnapshotEvent = delegate { };

    private void OnEnable()
    {
        _pointerGameCamera.gameObject.SetActive(false);
        //_backButton.gameObject.SetActive(false);
    }

    private void Start()
    {
       // _backButton.onClick.AddListener(ExitPointerMiniGame);
    }
    private void OnMouseDown()
    {
        GameScript.Get().Player.isInPointerGame = true;
        Cursor.lockState = CursorLockMode.None;
        _pointerGameCamera.gameObject.SetActive(true);
        //_backButton.gameObject.SetActive(true);
        SwitchSnapshotEvent(1);
    }
    private void OnMouseOver()
    {
        if (_PlayButton.GamePlay)
        {
            _PlayButton.GamePlay = false;
            _PlayButton.gameObject.SetActive(true);
            _PlayButton._Way.SetActive(false);
            SwitchSnapshotEvent(1);
        }
    }

    private Vector3 mousePosition;

    public void ExitPointerMiniGame()
    {
        _pointerGameCamera.gameObject.SetActive(false);
       // _backButton.gameObject.SetActive(false);
        SwitchSnapshotEvent(0);
        Cursor.lockState = CursorLockMode.Locked;

    }
}
