﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitButton : MonoBehaviour
{
    [SerializeField] private PlayButton _PlayButton;
    [SerializeField] private GameObject _Number;

    private void OnMouseOver()
    {
        if (_PlayButton.GamePlay)
        {
            _PlayButton.GamePlay = false;

            _PlayButton._Way.SetActive(false);
            gameObject.SetActive(false);

            _Number.SetActive(true);
        }
    }
}
