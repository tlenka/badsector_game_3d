﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Inventory : MonoBehaviour
{
    public List<ItemToCollect> CollectedItems;
    public InventorySlot[] _Slots;
    public InventorySlot SlotPrefab;
    //private Image[] _SlotSprite;
    [SerializeField] public List<ItemToCollect> ItemsToCollect;

    private static Inventory _invetory;
    public static Inventory Get()
    {
        return _invetory;
    }

    private Inventory() {
        _invetory = this;
        ItemsToCollect = new List<ItemToCollect>();
    }

    public void Prepare()
    {
        CollectedItems = new List<ItemToCollect>();
        Debug.Log("START " + CollectedItems.Count);
        //_Slots = FindObjectsOfType<InventorySlot>();
        _Slots = new InventorySlot[6];

        var slotPosition = SlotPrefab.transform.position;

        for (int i = 0; i < _Slots.Length; i++)
        {
            _Slots[i] = GameObject.Instantiate(SlotPrefab, slotPosition, Quaternion.identity, GameObject.FindObjectOfType<Canvas>().transform);

            slotPosition.x += 60f;

        }

        for (int i = 0; i < ItemsToCollect.Count; i++)
        {
            ItemsToCollect[i].index = i;
        }
    }
    //private void Start()
    //{
       
       
    //}

    public void AddItem(ItemToCollect item)
    {
        CollectedItems.Add(item);
        foreach (InventorySlot slot in _Slots)
        {
            Debug.Log(slot.name + " _______________ ");
            if (slot.Item == null)
            {
                slot.Item = item;
                slot.ItemImage.sprite = item.Image;
                slot.ItemImage.color = Color.white;
                break;
            }
        }
        //if (!item.IsCollected)
        //{

        //    CollectedItems.Add(item);
        //    foreach (InventorySlot slot in _Slots)
        //    {
                
        //        if (slot.Item == null)
        //        {
        //            slot.Item = item;
        //            slot.ItemImage.sprite = item.Image;
        //            slot.ItemImage.color = Color.white;
        //            break;
        //        }
        //    }
        //}
        //else
        //{
        //    foreach (InventorySlot slot in _Slots)
        //    {
        //        if (slot.Item != null)
        //        {
        //            Color color = slot.ItemImage.color;
        //            color.a = 1f;
        //            slot.ItemImage.color = color;
        //        }
                    
        //    }
        //}

        

        item.IsCollected = true;
    }

    public void RemoveItem(ItemToCollect item)
    {
        CollectedItems.Remove(item);
        foreach (InventorySlot slot in _Slots)
        {

            if (slot.Item == item)
            {
                slot.Item = null;
                slot.ItemImage.sprite = null;
                slot.ItemImage.color = Color.black;
                break;
            }
        }

        item.IsCollected = false;
    }
}
