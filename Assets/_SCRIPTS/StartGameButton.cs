﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class StartGameButton : MonoBehaviour
{
    public event Action Click = delegate { };
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        Debug.Log("new game btn ");
        Click();
    }

    
}
