﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Range(1f, 100f)] [SerializeField] float movementSpeed = 3f;
    [Range(1f, 200f)] [SerializeField] float rotationSpeed = 90f;

    [SerializeField] private float stepRate = 0.5f;
    private float stepCoolDown;

    public event Action FootStepAudioEvent = delegate { };
    public event Action RotationAudioEvent = delegate { };


    public Vector3 startPosition;
    public Quaternion startRotation;

    private Collider playerCollider;
    private Rigidbody playerRb;
    public Camera playerCamera;
    private Camera carCamera;
    private Transform movingObject;

    public bool isInCar;
    public bool isInPointerGame = false;
    [SerializeField] private Transform carTransform;
    public CarController carController;

    public CameraController cameraController;
    private float playerRotationY;

    private GameObject cursor;
    public bool canMove = true;

    private GameScript gameScript;

    public PointerMiniGame pointerMiniGame;

    private void Awake()
    {
        startPosition = this.gameObject.transform.position;
        startRotation = this.gameObject.transform.rotation;

        playerCollider = transform.GetComponent<Collider>();
        playerRb = transform.GetComponent<Rigidbody>();
        playerCamera = transform.GetComponentInChildren<Camera>();
        carCamera = carTransform.GetComponentInChildren<Camera>();

        carCamera.enabled = false;
        movingObject = transform;

        playerRotationY = transform.eulerAngles.y;
        cameraController = GetComponent<CameraController>();

        cursor = CanvasController.Get().cursor;
        gameScript = GameScript.Get();
        gameScript.Player = this;
        //playerRotation = new Vector3(0, transform.rotation.y, 0);
    }

    private void OnEnable()
    {
        carController.ClickCar += CarControllerClickCar_EventHandler;
        carController.ExitCar += CarControllerExitCar_EventHandler;
    }
    private void Start()
    {
        //Cursor.visible = true;
    }
    private void Update()
    {
        if (canMove)
        {
            stepCoolDown -= Time.deltaTime;
            if (Input.GetAxis("Vertical") != 0)
            {
                var z = Input.GetAxis("Vertical") * Time.deltaTime * movementSpeed;
                movingObject.Translate(0, 0, z);

                if (stepCoolDown < 0f)
                {
                    FootStepAudioEvent();
                    stepCoolDown = stepRate;
                }

            }

            if (isInCar)
            {
                if (Input.GetAxis("Horizontal") != 0)
                {
                    var x = Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed;
                    movingObject.Rotate(0, x, 0);

                    if (stepCoolDown < 0f)
                    {
                        RotationAudioEvent();
                        stepCoolDown = stepRate;
                    }
                }
            }
            else
                LookAtMouse();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PlayerEsc();
            Debug.Log("ESCAPE ____________  ");
            //CanvasController.Get().isCameraBack = true;
            
        }
    }

    private void PlayerEsc()
    {
        if (isInCar)
        {
            carController.Exit();
        }
        else if (isInPointerGame)
        {
            pointerMiniGame.ExitPointerMiniGame();
            isInPointerGame = false;
        }
        else
        {
            gameScript.CanvasController.TogglePauseMenu();
        }
    }
    //if (isInCar)
    //{
    //    if (Input.GetAxis("Vertical") != 0)
    //    {
    //        var z = Input.GetAxis("Vertical") * Time.deltaTime * movementSpeed;
    //        carTransform.Translate(0, 0, z);

    //        if (stepCoolDown < 0f)
    //        {
    //            FootStepAudioEvent();
    //            stepCoolDown = stepRate;
    //        }

    //    }

    //    if (Input.GetAxis("Horizontal") != 0)
    //    {
    //        var x = Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed;
    //        carTransform.Rotate(0, x, 0);

    //        if (stepCoolDown < 0f)
    //        {
    //            RotationAudioEvent();
    //            stepCoolDown = stepRate;
    //        }
    //    }
    //}
    //else
    //{
    //    stepCoolDown -= Time.deltaTime;
    //    if (Input.GetAxis("Vertical") != 0)
    //    {
    //        var z = Input.GetAxis("Vertical") * Time.deltaTime * movementSpeed;
    //        transform.Translate(0, 0, z);

    //        if (stepCoolDown < 0f)
    //        {
    //            FootStepAudioEvent();
    //            stepCoolDown = stepRate;
    //        }

    //    }
    //    if (Input.GetAxis("Horizontal") != 0)
    //    {
    //        var x = Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed;
    //        transform.Rotate(0, x, 0);

    //        if (stepCoolDown < 0f)
    //        {
    //            RotationAudioEvent();
    //            stepCoolDown = stepRate;
    //        }
    //    }
    //}



    //}

    private void LookAtMouse()
    {
        playerRotationY += Input.GetAxis("Mouse X");
        Quaternion rotation = Quaternion.Euler(0, playerRotationY * cameraController.lookSpeed, 0);
        transform.rotation = rotation;
    }

    private void CarControllerClickCar_EventHandler()
    {
        carCamera.enabled = true;
        playerCamera.enabled = false;
        movingObject = carTransform;
        playerCollider.enabled = false;
        playerRb.isKinematic = true;
        transform.SetParent(carTransform);
        isInCar = true;
        //cursor.SetActive(false);

        transform.position = new Vector3(carTransform.position.x, transform.position.y, carTransform.position.z);
        Quaternion rotation = Quaternion.Euler(0, carTransform.rotation.y, 0);
        transform.rotation = rotation;

        Cursor.visible = false;

    }

    private void CarControllerExitCar_EventHandler()
    {
        carCamera.enabled = false;
        playerCamera.enabled = true;
        movingObject = transform;
        playerCollider.enabled = true;
        playerRb.isKinematic = false;
        transform.SetParent(null);
        isInCar = false;
        //cursor.SetActive(true);
        Cursor.visible = true;
    }
}
