﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NumberController : MonoBehaviour
{
    [SerializeField] private Number num1;
    [SerializeField] private Number num2;
    [SerializeField] private Number num3;
    [SerializeField] private int _CorrectNum1;
    [SerializeField] private int _CorrectNum2;
    [SerializeField] private int _CorrectNum3;
    [SerializeField] private GameObject _Door;
    [SerializeField] private GameObject _DoorLightColor;
    [SerializeField] private Color _LightGreen;
   
    public static bool _DoorOpened = false;

    private void Start()
    {
        rend = _DoorLightColor.GetComponent<Renderer>();
        _DoorLight = _Door.GetComponent<Light>();
    }

    private Light _DoorLight;
    private Renderer rend;

    public void CheckCode()
    {
        if (num1.number == _CorrectNum1 && num2.number == _CorrectNum2 && num3.number == _CorrectNum3)
        {
            _DoorOpened = true;
            rend.material.SetColor("_Color", Color.green);
            _DoorLight.color = _LightGreen;
        }

    }
    
}
