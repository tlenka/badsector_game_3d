﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingObject : MonoBehaviour
{
    private Rigidbody rigidbody;
    private float maxPositionY;
    private float minPositionY;

    //private float directionX;
   // private float directionZ;

    private Vector3 direction;

    private bool isMovingUp = false;

    private float signDirectionX = 1f;
    private float signDirectionZ = 1f;

    private Vector3 lastFrameVelocity;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        minPositionY = transform.localPosition.y;
        maxPositionY = minPositionY + 0.6f;

        direction = new Vector3(transform.position.x + 0.01f, maxPositionY + 0.5f, transform.position.z + 0.01f);
        isMovingUp = true;


        //Debug.Log(transform.position + " _________________START____________________________ DIRECTION _____________________________ " + direction);

    }

    private void Update()
    {
        if (transform.position.y > maxPositionY)
        {
            isMovingUp = false;
        }

        if (transform.position.y < minPositionY)
        {
            isMovingUp = true;
        }


        if (isMovingUp)
        {
            direction.y = maxPositionY + 0.5f;
        }
        else
        {
            direction.y = minPositionY - 0.5f;

        }

        direction += new Vector3(signDirectionX * 0.005f, 0, signDirectionZ * 0.005f);


        transform.position = Vector3.Lerp(transform.position, direction, Time.deltaTime * 0.8f);


        //Debug.Log(direction + "=============== vvvvvvvvvvvvvvvvvvvvvvvvvv =========================== ");
        // lastFrameVelocity = rigidbody.velocity;
    }

    private void OnCollisionEnter(Collision collision)
    {

        var collisionNormal = collision.GetContact(0).normal;

        var reflectedPosition = Vector3.Reflect(direction, collisionNormal);
      //  Debug.Log(transform.position + " _________________ " + transform.position + collisionNormal + " ______________________ " + collisionNormal);



        signDirectionX = Mathf.Sign(collisionNormal.x);
        signDirectionZ = Mathf.Sign(collisionNormal.z);

        //direction = reflectedPosition;
        direction = new Vector3(transform.position.x + (signDirectionX * 0.005f), direction.y, transform.position.z + (signDirectionZ * 0.005f));

        //direction = new Vector3(reflectedPosition.x, direction.y, reflectedPosition.z);

       // Debug.Log(transform.position + " _____________________________________________ DIRECTION _____________________________ " + direction);

        //direction.x = direction.x
        // Debug.Log(transform.position + " ========================================== " + reflectedPosition);

        //direction = new Vector3(transform.localPosition.x + Mathf.Sign(reflectedPosition.x) * transform.localPosition.x + Mathf.Sign(reflectedPosition.x), direction.y, transform.localPosition.z + Mathf.Sign(reflectedPosition.z) * transform.localPosition.z + Mathf.Sign(reflectedPosition.x));
        //direction = new Vector3(direction.x * (-1f), direction.y, direction.z * (-1f));
        //Debug.Log(direction + " __________________ vvvvvvvvvvvvvvvvvv _____________________ " + collision.collider.name);
        //Bounce(collision.contacts[0].normal);
    }

}
