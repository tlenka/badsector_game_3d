﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Holder : MonoBehaviour
{
    [SerializeField] private AudioMixerSnapshot _isPlaying;
    [SerializeField] private AudioMixerSnapshot _isMute;

    [SerializeField][Range(0, 5f)] private float _holdersRaycastLength;

    private ItemToCollect[] _ArtItems;

    [SerializeField] public ItemToCollect _CorrectArtItem;

    public ArtTableController ArtTable;
    public bool IsCorrect;

    public event Action<AudioMixerSnapshot, float> PutOnHolderEvent = delegate { };
    public event Action PutItemEvent = delegate { };

        

    private void Start()
    {
       // _CorrectArtItem.gameObject.tag = _CorrectArtItem.gameObject.name;
        ArtTable = GetComponentInParent<ArtTableController>();
        _ArtItems = ArtTable.ArtItems;
        //_ArtItems = ArtTable.gameObject.GetComponentsInChildren<ItemToCollect>();

        
    }

    private void FixedUpdate()
    {
        HoldersRaycast();
    }

    private void OnMouseDown()
    {
        //Debug.Log(_ArtItems.Length);
        //_ArtItems = ArtTable.ArtItems;
        foreach (ItemToCollect artItem in _ArtItems)
        {
            //var item = artItem.gameObject.GetComponent<ItemToCollect>();

            if (artItem.IsSelected)
            {
                //Debug.Log("art item is selected");
                artItem.gameObject.transform.position = transform.position + new Vector3(0, gameObject.transform.lossyScale.y, 0);
                artItem.gameObject.transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);

                //artItem.gameObject.transform.rotation = Quaternion.identity;
                artItem.gameObject.SetActive(true);
                artItem.IsCollected = false;
                artItem.IsSelected = false;
                PutItemEvent();

                Inventory.Get().RemoveItem(artItem);




                if (artItem.Equals(_CorrectArtItem))
                {
                    IsCorrect = true;
                    //artItem.IsSelected = false;
                    PutOnHolderEvent(_isPlaying, 0.8f);

                    //if (ArtTable.IsCompleted)
                    //{
                    //    BlockAllArtItems();
                    //}

                }
                //else
                //{
                //    IsCorrect = false;
                //}

                artItem.IsSelected = false;
                break;
            }

            
        }
    }

    void BlockAllArtItems()
    {
        foreach (ItemToCollect artItem in _ArtItems)
        {
            artItem.CanClick = false;
        }
    }

    //private ArtItem[] _ArtItems;

    public event Action HoldersRayChangeEvent = delegate { };


    private void HoldersRaycast()
    {
        RaycastHit hit;
        //Vector3 rayOrigin = new Vector3(0, 1.7f, 0);

        var rayUp = Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out hit, _holdersRaycastLength);

        // Debug.Log(hit.collider);
        if (!rayUp)
        {
            if (IsCorrect)
            {
                PutOnHolderEvent(_isMute, 0.5f);
                IsCorrect = false;
            }

        }



    }

    void OnDrawGizmosSelected()
    {
        // Draws a 5 unit long red line in front of the object
        Gizmos.color = Color.red;
        Vector3 direction = transform.TransformDirection(Vector3.up) * _holdersRaycastLength;
        Gizmos.DrawRay(transform.position, direction);
    }

}
