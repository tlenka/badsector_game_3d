Space Invaders Couch information



Units of measurement
	This model was created using metric units and 1 unit = 1 meter.  The scale will have to be adjusted for other units of measure.
	1 meter = 100 cm = 3.28 feet = 39.37 inches




Textures and materials


	No textures are included with this model.




	All materials used raytraced reflections for the preview images.  The amount of reflection will have to be adjusted depending on the rendering software used and lighting setup for the scene.


	Raytrace reflections for each material

	black: 5%
	white: 10%




Vertices and faces


	Vertices: 4256
	Polygons: 3769


